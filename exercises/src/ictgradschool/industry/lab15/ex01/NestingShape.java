package ictgradschool.industry.lab15.ex01;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mshe666 on 8/01/2018.
 */
public class NestingShape extends Shape{
    private List<Shape> _children;

    public NestingShape() {
        super();
        _children = new ArrayList<Shape>();
    }

    public NestingShape( int x , int y ) {
        super(x, y);
        _children = new ArrayList<Shape>();
    }

    public NestingShape( int x , int y , int deltaX , int deltaY ) {
        super(x, y, deltaX, deltaY);
        _children = new ArrayList<Shape>();
    }

    public NestingShape( int x , int y , int deltaX , int deltaY , int width , int height ) {
        super(x, y, deltaX, deltaY, width, height);
        _children = new ArrayList<Shape>();
    }

    public void move( int width , int height ) {
        for (Shape s : _children) {

            s.move(this.getWidth(), this.getHeight());
        }
        super.move(width, height);
    }

    @Override
    public void paint(Painter painter) {
        painter.drawRect(fX,fY,fWidth,fHeight);
        for (Shape s : _children) {
            painter.translate(fX, fY);
            s.paint(painter);
            painter.translate(-fX, -fY);
        }
    }

    public void add(Shape child ) throws IllegalArgumentException {
        _children.add(child);
        child.setParent(this);
    }

    public void remove(Shape child ) {
        _children.remove(child);
        child.setParent(null);
    }

    public Shape shapeAt( int index ) throws IndexOutOfBoundsException {
        return _children.get(index);
    }

    public int shapeCount() {
        return _children.size();
    }

    public int indexOf(Shape child ) {
        return _children.indexOf(child);
    }

    public boolean contains(Shape child ) {
        return _children.contains(child);
    }

    public Shape getShape( int index ) {
        Shape result = null;
        try {
            result = _children.get( index );
        }
        catch( IndexOutOfBoundsException e ) {
            // No action necessary other than to catch the exception.
        }
        return result;
    }

    public int getChildCount() {
        return _children.size();
    }
}
