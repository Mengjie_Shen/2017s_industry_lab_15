package ictgradschool.industry.lab15.ex02;

import ictgradschool.industry.lab15.ex01.NestingShape;
import ictgradschool.industry.lab15.ex01.Shape;

import javax.swing.event.TreeModelListener;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by mshe666 on 8/01/2018.
 */
public class TreeModelAdapter implements TreeModel {

    private NestingShape _adaptee;
    private List<TreeModelListener> _treeModelListeners;

    public TreeModelAdapter(NestingShape root) {
        this._adaptee = root;
        this._treeModelListeners = new ArrayList<>();
    }

    @Override
    public Object getRoot() {
        return _adaptee;
    }

    @Override
    public Object getChild(Object parent, int index) {
        NestingShape nestingShape;
        if (parent instanceof NestingShape) {
            nestingShape = (NestingShape)parent;
            return nestingShape.getShape(index);
        }
        return null;
    }

    @Override
    public int getChildCount(Object parent) {
        NestingShape nestingShape;
        if (parent instanceof NestingShape) {
            nestingShape = (NestingShape)parent;
            return nestingShape.getChildCount();
        }
        return 0;
    }

    @Override
    public boolean isLeaf(Object node) {
        if (node instanceof Shape) {
            return !(node instanceof NestingShape);
        }
        return false;
    }

    @Override
    public void valueForPathChanged(TreePath path, Object newValue) {

    }

    @Override
    public int getIndexOfChild(Object parent, Object child) {
        NestingShape nestingShape;
        Shape shape;
        if (parent instanceof NestingShape && child instanceof Shape) {
            nestingShape = (NestingShape)parent;
            shape = (Shape)child;
            return nestingShape.indexOf(shape);
        }

        return 0;
    }

    @Override
    public void addTreeModelListener(TreeModelListener l) {
        _treeModelListeners.add(l);
    }

    @Override
    public void removeTreeModelListener(TreeModelListener l) {
        _treeModelListeners.remove(l);
    }
}
